import eventlet
eventlet.monkey_patch()

from uart import Uart
import time
from threading import Thread
from flask import Flask, render_template
from flask_socketio import SocketIO, emit

app = Flask(__name__,static_folder="templates/assets", 
template_folder="templates")

app.debug = True

socketio = SocketIO(app)

port = '/dev/ttyUSB0'
ser = Uart(port, 115200)

thread = None


def background_thread():
	while True:
		data,length,result = ser.SerialReceive()
		
		if result:
			print(data)
			socketio.emit('message', {"message" : data}, namespace='/test')
		time.sleep(0.01)
	ser.closeSerial()

@app.route('/')
@app.route('/dashboard.html')
def index():
	return render_template('dashboard.html')

@app.route('/tables.html')
def tables():
	return render_template('tables.html')

@socketio.on('send', namespace='/test')
def send(message):
	print("messaggio ricevuto: ",message['data'])
	ser.sendMessage(message['data'])

@socketio.on('connect', namespace='/test')
def connect():	
	global thread
	if thread is None:
		thread = Thread(target=background_thread)
		thread.start()
	
if __name__ == '__main__':	
	socketio.run(app)