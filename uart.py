import serial
import struct
from time import sleep
import sys
import time



class Uart:

	def __init__(self,port, baudrate):
		self.__tilde = False
		self.__rxBuffer = []
		
		self.ser = serial.Serial(port,baudrate)
		
		self.ser.setDTR(False)
		sleep(0.1)
		self.ser.setDTR(True)
		
		self.ser.flush()

		self.__nextMessage = True

		self.__rxData = bytes()
		self.__length = 0
		self.__result = False

		self.__txPacket = bytes()

		self.message = tuple()

	
		
	def SerialReceive(self):
		if self.ser.inWaiting() > 0:
			b = self.ser.read(self.ser.inWaiting())
			for i in range(len(b)):
				if b[i] == 126 or self.__tilde:
					if not self.__tilde:
						self.__tilde = True
					self.__rxBuffer.append(b[i])
					if(len(self.__rxBuffer) > 1):		
						if(self.__rxBuffer[-1] == 10 and self.__rxBuffer[-2] == 13):
							dim = struct.unpack('<H',bytearray(self.__rxBuffer[1:3]))[0]
							if(len(self.__rxBuffer) == dim+7):
								self.message = struct.unpack('<cHB'+str(dim)+'BBcc',bytearray(self.__rxBuffer))
							else:
								self.AckResponse(0)
							self.__rxBuffer = []
							self.__tilde = False
		if len(self.message) > 0:
			data,length,result = self.AvailableMessage(self.message[1:len(self.message)-2])
			data = struct.unpack('<'+str(int(length/2))+'H',bytearray(data))
			self.message = tuple()
			return data,length,result
		return (),0,False			
	
	def ResetDTR(self):
		self.ser.setDTR(False)
		sleep(0.1)
		self.ser.setDTR(True)
	
	def closeSerial(self):
		self.ser.flush()
		self.ser.close()
		sys.exit(0)

	def checkChecksum(self,message):
		
		checksumValue = 0
	
		for x in message:
			checksumValue += x
		
		return True if (checksumValue & 0x00ff) == 0xff else False

	def AvailableMessage(self,message):
		rxData, length, result = (),0,False
		if self.checkChecksum(message[1:]):
			if message[1] != 0: #is not ack
				self.AckResponse(1)
				
				rxData = message[2:len(message)-1]
				length = message[0]
				result = True
			else:
				if message[2] != 1: #ack false
					print("Resend Message!")
					self.ser.write(self.__txPacket)
				else:
					#self.nextMessage = True
					print("ACK ricevuto!")
		else:
			self.AckResponse(0)
		
		return rxData,length,result
		
		
	def calcChecksum(self,buffer):
		
		sumValue =  0

		for x in buffer[3:]:
			sumValue += x	
		
		return 0xff - (sumValue & 0x00ff)	
	

	def AckResponse(self,typeResponse):
		
		buffer = list()
		
		typeResponse = str(typeResponse).encode('ascii')

		'''start delimiter'''
		buffer.append(126)

		'''len ack'''
		buffer[1:3] = struct.unpack('<BB',struct.pack('<H',len(typeResponse)))

		'''payload'''
		buffer.append(0)
		buffer.append(int(typeResponse))
		buffer.append(self.calcChecksum(buffer))

		'''end delimiter'''
		buffer.append(13)
		buffer.append(10)

		self.__txPacket = struct.pack('<8B',*buffer)

		self.ser.write(self.__txPacket)
		

	def sendMessage(self,data):

		buffer = list()
		
		data = str(data).encode('ascii')
		
		buffer.append(126)
		buffer[1:3] = struct.unpack('<BB',struct.pack('<H',len(data)))

		buffer.append(1)

		for x in data:
			buffer.append(x)

		buffer.append(self.calcChecksum(buffer))

		buffer.append(13)
		buffer.append(10)

		self.__txPacket = struct.pack('<B2BB'+str(len(data))+'BBBB',*buffer)
		
		self.ser.write(self.__txPacket)
		
		#self.__nextMessage = False
			

	def WaitingAck(self):
		return self.__nextMessage

	